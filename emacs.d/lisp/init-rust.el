;;; init-rust ---

;; Copyright (C) 2018 Charis-Nicolas Georgiou

;; Author: Charis-Nicolas Georgiou <cng_it@posteo.net>
;; Created: 11 Nov 2018
;; Version: 1.0
;; Keywords: rust emacs dotfiles
;; X-URL: https://github.com/mokshasoul/dotfiles

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;;; Code:
;;

(use-package rust-mode
  :mode ("\\.rs\\'" . rust-mode))

(use-package cargo
  :hook (rust-mode . cargo-minor-mode))

(use-package racer
  :after rust-mode
  :hook(
        (rust-mode . racer-mode)
        (racer-mode . eldoc-mode)
        (racer-mode . company-mode)
        )
  :config
  (setq racer-cmd "~/.cargo")
  (use-package company-racer
    :config
    (add-to-list 'company-backends 'company-racer)))
(use-package toml-mode
  :mode ("\\.toml"))
(use-package flycheck-rust
  :after flycheck
  :commands flycheck-rust-setup
  :init
  (add-hook 'flycheck-mode-hook #'flycheck-rust-setup))
(provide 'init-rust)

;;; init-rust.el ends here
