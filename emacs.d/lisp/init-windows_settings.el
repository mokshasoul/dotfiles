; ;;; init-windows_settings ---

;; Copyright (C) 2018 Charis-Nicolas Georgiou

;; Author: Charis-Nicolas Georgiou <cng_it@posteo.net>
;; Created: 04 Nov 2018
;; Version: 1.0
;; Keywords: windows-nt windows dotfiles
;; X-URL: https://github.com/mokshasoul/dotfiles

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary: includes windows specific settings

;;; Code:
;;

;; Taken from https://www.reddit.com/r/emacs/comments/8s2ygn/best_practices_for_emacs_on_microsoft_windows/e1082is
(defmacro help/on-windows (statement &rest statements)
  "Evaluate the enclosed body only when run on Microsoft Windows."
  `(when (eq system-type 'windows-nt)
     ,statement
     ,@statements))

;; To use this you need to cinst -y gnuwin32-coreutils.portable
(help/on-windows
 (setq projectile-indexing-method 'alien))


(help/on-windows
 (set-clipboard-coding-system 'utf-16le-dos))


(provide 'init-windows_settings)

;;; init-windows_settings.el ends here
