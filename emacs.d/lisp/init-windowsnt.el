;;; init-windowsnt --- Emacs configuration: Windows-NT specific settings and workarounds

;; Copyright (C) 2018 Charis-Nicolas Georgiou

;; Author: Charis-Nicolas Georgiou <cng_it@posteo.net>
;; Created: 20 Mai 2018
;; Version: 1.0
;; Keywords: emacs dotfiles powershell windows
;; X-URL: https://github.com/mokshasoul/dotfiles

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;;; Code:
;;

;;; Powershell  to be used by all
;; Taken from https://www.reddit.com/r/emacs/comments/8s2ygn/best_practices_for_emacs_on_microsoft_windows/e1082is/
(defmacro help/on-windows (statement &rest statements)
  "Evaluate the enclosed body only when run on Microsoft Windows."
  `(when (eq system-type 'windows-nt)
     ,statement
     ,@statements))

(use-package powershell)
;; To use this you need to cinst -y gnuwin32-coreutils.portable
(help/on-windows
 (setq projectile-indexing-method 'alien))

(help/on-windows
 (setq shell-file-name "cmdproxy.exe"))

;; Workaround for error under windows when switching to submodule
;; based repository
(help/on-windows
 (setq projectile-git-submodule-command nil))

(help/on-windows
 (define-coding-system-alias 'cp65001 'utf-8))

(help/on-windows
 (set-clipboard-coding-system 'utf-16le-dos))

(help/on-windows (setq help/font-size-current 13))

(help/on-windows
 (tool-bar-add-item
  "windows"
  'do-nothing
  'Powered-by-Microsoft-Windows
  :help "Powered by Microsoft Windows"
  :enable nil))

(provide 'init-windowsnt)

;;; init-windowsnt.el ends here
