;;; init-org --- Emacs configuration: org-mode

;; Copyright (C) 2018 Charis-Nicolas Georgiou

;; Author: Charis-Nicolas Georgiou <cng_it@posteo.net>
;; Created: 16 Mai 2018
;; Version: 1.0
;; Keywords: org-mode emacs dotfiles
;; X-URL: https://github.com/mokshasoul/dotfiles

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;;; Code:
;;
(require 'org)
(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
(require 'org-protocol)
;; Steve purcell org settings
;; Various preferences
(setq org-log-done t
      org-refile-targets '((nil :maxlevel . 9)
                           (org-agenda-files :maxlevel . 9))
      org-outline-path-complete-in-steps nil
      org-refile-use-outline-path t
      org-edit-timestamp-down-means-later t
      org-archive-mark-done nil
      org-hide-emphasis-markers t
      org-catch-invisible-edits 'show
      org-export-coding-system 'utf-8
      org-fast-tag-selection-single-key 'expert
      org-html-validation-link nil
      org-export-kill-product-buffer-when-displayed t
      org-tags-column 4
      org-directory '("~/_org/")
      org-agenda-files '("~/_org/")
      org-default-notes-file "~/_org/notes.org")
;; inserts full filename at top of file to link different org files
;; (use-package org-fstree
;;   :ensure t)
;; ****************************************************************
;; GTD Configuration
;; ****************************************************************
(setq org-todo-keywords
      '((sequence "TODO(t)" "STARTED(s)" "WAITING(w)" "Remember(r)" "|" "DONE(d)" "CANCELED(c)")))


;; link from clip
(use-package org-cliplink
  :no-require
  :config
  (define-key org-mode-map (kbd "C-c M-l") 'org-cliplink))

;;; Cool Bullets
(use-package org-bullets
  :init
  (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

;;; Set Org-Capture templates
(setq org-capture-templates
      '( ("p" "Protocol" entry (file+headline "~/_org/notes.org" "InboxURL")
          "* %^{Title}\nSource: %u, %c\n #+BEGIN_QUOTE\n%i\n#+END_QUOTE\n\n\n%?")
         ("L" "Protocol Link" entry (file+headline "~/_org/notes.org" "InboxURL")
          "* %? [[%:link][%:description]] \nCaptured On: %U")
         ("t" "todo" entry
          (file+headline "~/_org/tasks.org" "Tasks")
          "** TODO %^{Brief description} %^g\n%?\nAdded: %U")
         ("n" "note" entry
          (file+headline "~/_org/notes.org" "Notes")
          "** :NOTE:\n%U\n%a\n" :clock-keep t)
         ("h" "habit" entry
          (file+headline "~/_org/tasks.org" "Habits")
          "** TODO %^{Brief description}%^t")))


(define-key global-map (kbd "C-c l") 'org-store-link)
(define-key global-map (kbd "C-c c") 'org-capture)
(define-key global-map (kbd "C-c a") 'org-agenda)
;; Lots of stuff from http://doc.norang.ca/org-mode.html
;;; integrate projectile todos with org-todos
(use-package org-projectile
  :after (org projectile)
  :bind (("C-c n p" . org-projectile-project-todo-completing-read))
  :config
  (progn
    (setq org-projectile-projects-file
          "~/Documents/_org/projects.org")
    (setq org-agenda-files (append org-agenda-files (org-projectile-todo-files)))
    (push (org-projectile-project-todo-entry) org-capture-templates)))

;; (use-package org-protocol)
;; Encrypt org files after save :crypt: tag
(require 'org-crypt)
(org-crypt-use-before-save-magic)
(setq org-crypt-key "66755BC83D2B7EE9")

(use-package org-noter
  :commands org-noter)


;; Calendar packages for org
(use-package org-caldav
  :config
  ;; https://posteo.de:8443/calendars/cng_it/dbhasi/
  (setq-default org-caldav-url "https://posteo.de:8443/calendars/cng_it"
                org-caldav-calendar-id  "dbhasi"
                org-caldav-inbox "~/_org/posteo_inbox.org"
                org-caldav-files '("~/_org/calendar.org")
                org-icalendar-use-deadline '(event-if-todo)
                org-icalendar-timezone "Europe/Berlin"))
(use-package calfw)
(use-package calfw-org
  :after calfw)

(provide 'init-org)

;;; init-org.el ends here
