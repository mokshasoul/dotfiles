;;; init-js --- Emacs configuration: org-mode

;; Copyright (C) 2018 Charis-Nicolas Georgiou

;; Author: Charis-Nicolas Georgiou <cng_it@posteo.net>
;; Created: 16 Mai 2018
;; Version: 1.0
;; Keywords: org-mode emacs dotfiles
;; X-URL: https://github.com/mokshasoul/dotfiles

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:
;;; Code:
;;; Currently top javascript package for emacs
(use-package js2-mode
  :ensure t
  :mode (("\\.js\\'" . js2-mode)
	 ("\\.pac\\'" . js2-mode))
  :commands js2-mode
  :interpreter "node"
  :config
  (setq mode-name "JS2")
  (js2-imenu-extras-mode t)
  (setq-default
   js2-mode-indent-ignore-first-tab t))

;; JSON support
(use-package json-mode
  :ensure t
  :commands json-mode
  :config
  (bind-keys :map json-mode-map
             ("C-c <tab>" . json-mode-beautify)))
;; Adddinsx
(use-package json-snatcher :ensure t)
(use-package js-doc :ensure t)

(provide 'init-js)
;;; init-js ends here
