;;; init-latex ---  LaTeX emacs configuration

;; Copyright (C) 2018 Charis-Nicolas Georgiou

;; Author: Charis-Nicolas Georgiou <cng_it@posteo.net>
;; Created: 25 Nov 2018
;; Version: 1.0
;; Keywords:latex emacs lisp
;; X-URL: https://github.com/mokshasoul/dotfiles

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;;; Code:
;;

(use-package auctex
  :mode ("\\.tex\\'" . TeX-latex-mode)
  :hook ((LaTeX-mode . visual-line-mode)
         (LaTeX-mode . LaTeX-math-mode)
         (LaTeX-mode . auto-fill-mode)
         (LaTeX-mode . turn-on-reftex)
         (LaTeX-mode . flyspell-mode))
  :custom
  (custom-set-variables '(LaTeX-command "latex -synctex=1"))
  :init
    (add-hook 'TeX-after-compilation-finished-functions
              #'TeX-revert-document-buffer)

    (setq TeX-view-program-list
          '(("PDF Viewer" "okular --unique %o#src:%n%b")))
    (setq TeX-view-program-selection
          '((output-pdf "PDF Viewer"))))

(provide 'init-latex)
;;; init-latex.el ends here
