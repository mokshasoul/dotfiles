;;; init-ivy ---  ivy configuration
;; Copyright (C) 2018 Charis-Nicolas Georgiou
;; Author: Charis-Nicolas Georgiou <cng_it@posteo.net>
;; Created: 30 Apr 2018
;; Version: 1.0
;; Keywords: ivy swyper counsel
;; X-URL: https://github.com/mokshasoul/dotfiles
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 2, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:
;;; This file describes my personal ivy, counsel and swiper config
;;; Note that swiper actually includes both, but this is done for cleaness

;;; Code:
(use-package ivy
  :diminish
  :bind (("C-x b" . ivy-switch-buffer)
         ("C-c C-r" . ivy-resume)
         ("C-x B" . ivy-switch-buffer-other-window))
  :custom
  (ivy-dynamic-exhibit-delay-ms 200)
  (ivy-height 10)
  (ivy-initial-input-alist nil t)
  (ivy-use-virtual-buffers t)
  (ivy-magic-tilde nil)
  (ivy-virtual-abbreviate 'fullpath)
    (ivy-count-format "")
  :config
  (ivy-mode 1)
  (ivy-set-occur 'ivy-switch-buffer 'ivy-switch-buffer-occur))
(use-package counsel
  :after ivy
  :diminish
  :bind (
         ("C-*" . counsel-org-agenda-headlines)
         ("M-x" . counsel-M-x)
         ("C-x C-f" . counsel-find-file)
         ("C-x C-f" . counsel-find-file))
  :commands counsel-minibuffer-history
  :custom
  (global-set-key (kbd "<f1> f") 'counsel-describe-function)
  (global-set-key (kbd "<f1> v") 'counsel-describe-variable)
  (global-set-key (kbd "<f1> l") 'counsel-find-library)
  (global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
  (global-set-key (kbd "<f2> u") 'counsel-unicode-char)
  (global-set-key (kbd "C-c g") 'counsel-git)
  (global-set-key (kbd "C-c j") 'counsel-git-grep)
  (global-set-key (kbd "C-c k") 'counsel-ag)
  (global-set-key (kbd "C-x l") 'counsel-locate)
  (define-key read-expression-map (kbd "C-r") 'counsel-expression-history))

;; (use-package counsel-tramp
;;   :commands counsel-tramp)

(use-package swiper
  :bind (("C-s" . swiper)
         ("C-r" . swiper))
  :config
  (progn
    (setq-default
     magit-completing-read-function 'ivy-completing-read
     enable-recursive-minibuffers t)))

(use-package ivy-xref
  :config
  (setq xref-show-xrefs-function 'ivy-xref-show-xrefs))


(provide 'init-ivy)
;;; init-ivy.el ends here
