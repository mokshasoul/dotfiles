(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(delete-selection-mode nil)
 '(package-selected-packages
   (quote
    (powershell lsp-ui company-lsp lsp-mode flycheck-rust company-racer toml-mode racer cargo org-protocol crux yasnippet-snippets yagist whitespace-cleanup-mode which-key wgrep web-mode vlf virtualenvwrapper use-package unfill undo-tree try tagedit systemd symbol-overlay switch-window smarty-mode session scss-mode scratch sass-mode regex-tool rainbow-delimiters pipenv paredit-everywhere page-break-lines org-projectile org-cliplink org-bullets nov nlinum multiple-cursors mode-line-bell markdown-mode magit-gh-pulls lua-mode list-unicode-display leuven-theme js2-mode js-doc ivy-xref i3wm htmlize gnuplot git-timemachine ggtags fullframe flycheck-color-mode-line expand-region exec-path-from-shell emmet-mode elpy editorconfig dynamic-spaces dsvn dotenv-mode dockerfile-mode docker-compose-mode docker diredfl diminish diff-hl csv-mode counsel-tramp counsel-projectile company-php company-jedi company-auctex company-ansible command-log-mode calfw-org calfw browse-kill-ring beacon ansible-doc ansible ace-window)))
 '(safe-local-variable-values (quote ((engine . django))))
 '(session-use-package t nil (session)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(aw-leading-char-face ((t (:inherit ace-jump-face-foreground :height 3.0)))))
